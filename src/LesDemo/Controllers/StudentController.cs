﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LesDemo.Models;
using MyHowest;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesDemo.Controllers
{
    public class StudentController : Controller
    {
        public IActionResult Index()
        {
                
            return View(new Student() { Id = 300, Naam = "Johnny", Afstudeergraad = Graad.Voldoende });
        }

    
    }
}
